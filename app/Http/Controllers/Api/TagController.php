<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index (Request $request)
    {
        $tags = Tag::orderBy('name');

        return response()->json([
            'response' => 200,
            'tags' => $tags,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store (Request $request)
    {
        $data = TagRequest::validate($request);

        if ($data['status']) {
            $data['data']['user_id'] = auth()->id ?? 1;
            $tag = Tag::create($data['data']);
        } else {
            return response()->json([
                'response' => 422,
                'tag' => $data['data'],
                'message' => $data['message'],
            ], 422);
        }

        return response()->json([
            'response' => 200,
            'tag' => $tag,
            'message' => 'Tag created successfully',
        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show (Request $request)
    {
        $tag = Tag::find($request['tag']);

        TagRequest::checkEmpty($tag);
        TagRequest::checkPermission($tag);

        return response()->json([
            'response' => 200,
            'tag' => $tag,
            'message' => 'Tag data is returned',
        ], 200);
    }
}
