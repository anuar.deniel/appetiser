<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index (Request $request)
    {
        $input = $request->only('sort', 'completed', 'priority', 'due_date', 'search', 'archive', 'tag');
        
        $tasks = Task::orderBy($input['sort'] ?? 'due_date');

        if (! empty($input['completed'])) {
            $tasks = $tasks->where('completed', 1);
        }
        
        if (! empty($input['priority'])) {
            $tasks = $tasks->whereIn('priority', $input['priority']);
        }
        
        if (! empty($input['due_date'])) {
            $dates = explode(',', trim(trim($input['due_date'], '['), ']'));
            $tasks = $tasks->whereBetween('due_date', $dates);
        }

        if (! empty ($input['archive'])) {
            $tasks = $tasks->where('archive', true);
        }

        if (! empty ($input['search'])) {
            $tasks = $tasks->where(function($query) {
                $query->where('title', 'LIKE', $input['search'] . '%')
                    ->orWhere('title', 'LIKE', '%' . $input['search'])
                    ->orWhere('title', 'LIKE', '%' . $input['search'] . '%');
            });
        }

        $tasks = $tasks->paginate('10');

        return response()->json([
            'response' => 200,
            'tasks' => $tasks,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store (Request $request)
    {
        $data = TaskRequest::validate($request);

        if ($data['status']) {
            $data['data']['user_id'] = auth()->id ?? 1;
            $task = Task::create($data['data']);
        } else {
            return response()->json([
                'response' => 422,
                'task' => $data['data'],
                'message' => $data['message'],
            ], 422);
        }

        return response()->json([
            'response' => 200,
            'task' => $task,
            'message' => 'Task created successfully',
        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show (Request $request)
    {
        $task = Task::find($request['task']);

        TaskRequest::checkEmpty($task);
        TaskRequest::checkPermission($task);

        return response()->json([
            'response' => 200,
            'task' => $task,
            'message' => 'Task data is returned',
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update (Request $request)
    {
        $data = TaskRequest::validate($request);

        if ($data['status']) {
            $task = Task::find($request->task);

            TaskRequest::checkEmpty($task);
            TaskRequest::checkPermission($task);

            $task->fill($data['data'])->save();
            return response()->json([
                'response' => 200,
                'task' => $task,
                'message' => 'Task data is updated',
            ], 200);
        } else {
            return response()->json([
                'response' => 422,
                'task' => $data['data'],
                'message' => $data['message'],
            ], 422);
        }
    }

    /**
     * Update the specified resource in storage to complete.
     */
    public function action (Request $request)
    {
        $task = Task::find($request['task']);

        TaskRequest::checkEmpty($task);
        TaskRequest::checkPermission($task);

        if ($request->action == 'complete') {
            if (is_null ($task->completed_date)) {
                $task->completed_date = now()->format('Y-m-d H:i:s');
                $message = 'completed';
            } else {
                $task->completed_date = null;
                $message = 'incomplete';
            }
            $task->save();
        }
        if ($request->action == 'archive') {
            if (is_null ($task->archived_date)) {
                $task->archived_date = now()->format('Y-m-d H:i:s');
                $message = 'archived';
            } else {
                $task->archived_date = null;
                $message = 'unarchived';
            }
            $task->save();
        }

        return response()->json([
            'response' => 200,
            'task' => $task,
            'message' => 'Task has been marked as ' . $message,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy (Request $request)
    {
        $task = Task::find($request['task']);

        TaskRequest::checkEmpty($task);
        TaskRequest::checkPermission($task);

        $task->delete();

        return response()->json([
            'response' => 204,
        ], 204);
    }
}
