<?php

namespace App\Http\Requests;

use App\Models\Tag;
use Illuminate\Http\Request;
use Exception;

class TagRequest
{
    /**
     * Validate request data
     * 
     * @param $requests
     * @return array
     */
    public static function validate($requests): Array
    {
        $return = [
            'message' => [
                'name' => '',
            ],
            'status' => 'true',
        ];

        if (empty ($requests->name)) {
            $return['message']['name'] = 'Tag name cannot be empty';
            $return['status'] = false;
        }
        
        return $return;
    }

    /**
     * Check tag existence
     * 
     * @param $tag
     * @return null|Exception
     */
    public static function checkEmpty ($tag)
    {
        if (! $tag) {
            throw new class extends Exception {
                public function render()
                {
                    return response()->json([
                        'response' => 404,
                        'tag' => [],
                        'message' => 'Tag not found. Please try again.',
                    ], 404);
                }
            };
        }
    }

    /**
     * Check tag permission
     * 
     * @param $tag
     * @return null|Exception
     */
    public static function checkPermission ($tag)
    {
        if ($tag && $tag->user_id != /*auth()->id*/ 1) {
            throw new class extends Exception {
                public function render()
                {
                    return response()->json([
                        'response' => 401,
                        'tag' => [],
                        'message' => 'You have no permission to update this tag.',
                    ], 401);
                }
            };
        }
    }
}
