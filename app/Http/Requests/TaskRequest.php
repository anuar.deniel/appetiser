<?php

namespace App\Http\Requests;

use App\Models\Task;
use App\Utilities\Enums\Priority;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Exception;
use ReflectionClass;

class TaskRequest
{
    /**
     * Validate request data
     * 
     * @param $requests
     * @return array
     */
    public static function validate($requests): Array
    {
        $reflection = new ReflectionClass(Priority::class);
        $priorities = $reflection->getConstants();

        $return = [
            'message' => [
                'title' => '',
                'description' => '',
                'priority' => '',
                'due_date' => '',
                'archive_date' => '',
                'completed_date' => '',
            ],
            'data' => [
                'title' => $requests->title,
                'description' => $requests->description,
                'priority' => $requests->priority,
                'due_date' => $requests->due_date,
                'archive_date' => $requests->archive_date,
                'completed_date' => $requests->completed_date,
            ],
            'status' => true,
        ];

        foreach ($requests->all() as $key => $value) {
            switch ($key) {
                case 'title':
                    if (empty ($value)) {
                        $return['message']['title'] = 'Title cannot be empty';
                        $return['status'] = false;
                    }
                    break;

                case 'description':
                    if (empty ($value)) {
                        $return['message']['description'] = 'Description cannot be empty';
                        $return['status'] = false;
                    }
                    break;
                
                case 'priority':
                    if (! in_array ($value, $priorities)) {
                        $return['message']['priority'] = 'Prioriry value is invalid';
                        $return['status'] = false;
                    }
                    break;
                
                case 'due_date':
                    if (Carbon::parse($value)->format('Y-m-d') > now()->format('Y-m-d')) {
                        $return['message']['due_date'] = 'Due date cannot be in the past';
                        $return['status'] = false;
                    }
                    break;
            }

            return $return;
        }
    }

    /**
     * Check task existence
     * 
     * @param $task
     * @return null|Exception
     */
    public static function checkEmpty ($task)
    {
        if (! $task) {
            throw new class extends Exception {
                public function render()
                {
                    return response()->json([
                        'response' => 404,
                        'task' => [],
                        'message' => 'Task not found. Please try again.',
                    ], 404);
                }
            };
        }
    }

    /**
     * Check task permission
     * 
     * @param $task
     * @return null|Exception
     */
    public static function checkPermission ($task)
    {
        if ($task && $task->user_id != /*auth()->id*/ 1) {
            throw new class extends Exception {
                public function render()
                {
                    return response()->json([
                        'response' => 401,
                        'task' => [],
                        'message' => 'You have no permission to update this task.',
                    ], 401);
                }
            };
        }
    }
}
