<?php

namespace App\Utilities\Enums;

abstract class Priority
{
    /** @var string */
    public const URGENT = 'urgent';

    /** @var string */
    public const HIGH = 'high';

    /** @var string */
    public const NORMAL = 'normal';

    /** @var string */
    public const LOW = 'low';
}
