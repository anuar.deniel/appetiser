# README #

This is a repository to host the Assessment for Full Stack Developer position in Appetiser Apps


### 1. What you need ###

```
PHP version 8.1+
MySQL version 5.7+
Composer version 2+
NPM version 8+
```

**Assumptions**

You would need a machine (Windows/Mac OS) with a webserver configured to serve a laravel application, with a working Mysql server.

Laravel uses composer to install package dependencies, and VueJS uses NPM to install its dependencies.


### 2. Clone this repository ###

If you haven't already, clone this repo to your working webserver http folder. 
For example, if you installed the web server using MAMP, your htdocs folder would be `/Applications/MAMP/htdocs/` (in MAC OS). 
If you use any other web server, make sure to clone this repo to its htdocs folder.


### 3. Set Database credentials ###

You need to set the credentials of your local database to let Laravel connect to it.

To do that, open up .env file (if you cant find the file, copy .env.example to .env), and change the following credentials to match your settings:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=anuar-deniel
DB_USERNAME=root
DB_PASSWORD=123456
```


### 4. Get packages dependencies ###

To get all the dependencies for the project, you need to run these commands from the terminal:

```
$ cd /Applications/MAMP/htdocs/anuardeniel      // this is where you cloned in Step 2
$ composer install
$ npm install
```


### 5. Migrate Tables & Seeding the Tables ###

When everything is in place, all that's left is to create the tables and seed initial data into your newly created database.
If you haven't already, create a database named `anuar-deniel` (or any other name you specify in Step 3).


**NOTE**: The collation for the database is preferably **utf8mb4_unicode_ci**.


When you are done, open up your terminal, and navigate to your cloned project folder. Then, run these commands:


```
$ php artisan migrate
$ npm run dev
```

### Thank you for taking the time to review my code ###
